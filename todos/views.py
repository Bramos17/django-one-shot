from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from todos.models import TodoList, TodoItem
from todos.forms import TodoItemForm, TodoListForm


@login_required
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_lists_list": todo_list,
    }
    return render(request, "todos/main-page.html", context)


@login_required
def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "list": list,
    }
    return render(request, "todos/details.html", context)


@login_required
def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=form.instance.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


@login_required
def todo_list_update(request, id):
    edit = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = TodoListForm(request.POST, instance=edit)
        if form.is_valid():
            edit = form.save()
            return redirect("todo_list_detail", id=edit.id)
    else:
        form = TodoListForm(instance=edit)
    context = {
        "form": form,
    }
    return render(request, "todos/update.html", context)


@login_required
def todo_list_delete(request, id):
    this_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        this_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


@login_required
def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/item/create.html", context)


@login_required
def todo_item_update(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm(instance=todoitem)
    context = {
        "form": form,
    }
    return render(request, "todos/item/update.html", context)
